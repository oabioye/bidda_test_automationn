///<reference types="cypress"/>
import AuctioneesPage from "../pageObjects/AuctioneesPage";


describe('Auctionees Page', () =>{

    const auctioneesPage = new AuctioneesPage()
    const email = "delo@yopmail.com"
    const password = "P@ssword1"
    const auctionTitle = ""

    beforeEach(() =>{
        cy.auctionee_login(email,password)
      })

    it('Should be able to place bids', () =>{
        auctioneesPage.acceptAuctionInvite(auctionTitle)
        
    })

})
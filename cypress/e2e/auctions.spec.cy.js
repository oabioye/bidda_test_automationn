///<reference types="cypress"/>
import AuctionsPage from "../pageObjects/AuctionsPage";
import AuctioneesPage from "../pageObjects/AuctioneesPage";


describe('Auctions Page', () => {

  const auctionsPage = new AuctionsPage()
  const auctioneesPage = new AuctioneesPage()
  const auction_title = "TOPTAL"
  const start_time = auctionsPage.getCurrentHour()
  const end_time = auctionsPage.getNextHour()
  const start_date = auctionsPage.getTodaysDate()
  const end_date = auctionsPage.getTodaysDate()
  const asset_title = "Mercedes Benz"
  const starting_bid = "20000"
  const max_bid = "30000"
  const min_increment = "1000"
  const quantity = "7"
  const description = "Automated Test Description"
  const contactTitle = "Rite Contact"
  const invitees = ["delo@yopmail.com","lilo@yopmail.com","obaloluwa.peter@rayda.co"]
  const email = "delo@yopmail.com"
  const password = "P@ssword12"

  beforeEach(() =>{
    cy.auctioneer_login()
  })
    it('should be able to create auctions', () => {
        auctionsPage.OpenAuctionsPage()
        auctionsPage.createAuctions(auction_title,start_date,end_date,start_time,end_time)
        auctionsPage.addAuctionItems(asset_title,starting_bid,max_bid,min_increment,quantity,description)
        //auctionsPage.createContactList(contactTitle,invitees)
        auctionsPage.addExistingContactList()
        auctionsPage.publishAuction(auction_title)
        auctionsPage.viewAuction(auction_title)
        auctionsPage.auctionGoLive(auction_title)
    })
    
    it('should be able to place bids', () => {
      cy.logout()
      cy.auctionee_login("abioyeobaloluwapeter@gmail.com","P@ssword1")
      auctionsPage.switchToAuctioneePage()
      cy.wait(500)
      auctioneesPage.acceptAuctionInvite(auction_title)
      for(let i=1; i<=3; i++){
        auctioneesPage.placeBid(i.toString())
        cy.wait(300)
      }
    })

    it('should be able to place bids for another user', () => {
      cy.logout()
      cy.auctionee_login(email,password)
      cy.wait(500)
      auctioneesPage.acceptAuctionInvite(auction_title)
      for(let i=3; i<=5; i++){
        auctioneesPage.placeBid(i.toString())
        cy.wait(300)
      }
    })

})
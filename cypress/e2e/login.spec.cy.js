///<reference types="cypress"/>

import LoginPage from "../pageObjects/LoginPage";


describe('Login Page', () => {
    const loginPage = new LoginPage();

    beforeEach(() => {
        loginPage.visit();
    });

    it('should be able to log in', () => {
        //cy.auctioneer_login()
        loginPage.fillEmail("obaloluwa.peter@rayda.co")
        loginPage.fillPassword("P@ssword2023")
        loginPage.submitLoginForm()
        loginPage.logout()
      
    })
})
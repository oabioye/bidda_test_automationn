///<reference types="cypress"/>


class AuctioneesPage{

    acceptAuctionInvite(auction_title){
        cy.contains(auction_title).click()
        const accept_btn = '//p[text()="Accept Invitation"]'
        cy.xpath(accept_btn).click()
        cy.contains('Auction Invitation Accepted').should('exist')
        // cy.get('body').then(($el) => {
        //     if($el.has(accept_btn)){
        //         cy.xpath(accept_btn).click()
        //         cy.contains('Auction Invitation Accepted').should('exist')
        //     }else{
        //         cy.contains(auction_title).should('exist')
        //     }
        // })
       
    }

    placeBid(asset_to_bid_id){
        cy.xpath('(//input[@name="price"])['+asset_to_bid_id+']').click()
        cy.xpath('(//button[text()="Place Bid"])['+asset_to_bid_id+']').click()
        cy.xpath('(//input[@name="price"])['+asset_to_bid_id+']').should(($input) => {
            const val = $input.val()

            console.log(val)
        })
        cy.wait(500)
    }
}


export default AuctioneesPage
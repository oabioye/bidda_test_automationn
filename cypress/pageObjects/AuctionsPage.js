///<reference types="cypress"/>
import 'cypress-file-upload'

class AuctionsPage{

    getTodaysDate(){
        const today = new Date();
        const yyyy = today.getFullYear();
        let mm = today.getMonth() + 1; // Months start at 0!
        let dd = today.getDate();
      
        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
      
        const formattedToday = dd + '/' + mm + '/' + yyyy;
        return formattedToday
    }

    getCurrentHour(){
        var new_hours = "0"
        const date = new Date();
        const hours = date.getHours()
        if(hours<10){
            new_hours = new_hours+hours
        }else{
            new_hours = hours
        }
        const current_time = new_hours+":00"
        
        return current_time
    }

    getNextHour(){
        var new_hours = "0"
        const date = new Date();
        const hours = date.getHours()+1
        if(hours<10){
            new_hours = new_hours+hours
        }else{
            new_hours = hours
        }
        const next_hour = new_hours+":00"
        return next_hour
    }


    OpenAuctionsPage(){
        cy.contains('Your current sales auctions and activities', {timeout: 10000})
        cy.xpath('(//a[@href="/auctioneer/auctions"])[1]').click()
    }

    createAuctions(auction_title, start_date, end_date, start_time, end_time){
        cy.xpath('//a[text()="Create Auction"]').click()
        cy.xpath('(//input[@type])[1]').clear().type(auction_title)
        cy.get('[id*="r2"]').type(start_date)
        cy.xpath('(//div[contains(@class,"MuiSelect-select")])[1]').click()
        cy.get('li[data-value="'+start_time+'"]').click()
        cy.get('[id*="r4"]').type(end_date)
        cy.xpath('(//div[contains(@class,"MuiSelect-select")])[2]').click()
        cy.get('li[data-value="'+end_time+'"]').click()
        cy.xpath('(//div[contains(@class,"MuiSelect-select")])[3]').click()
        cy.get('li[data-value*="Africa/Lagos"]').click()
        cy.xpath('(//div[contains(@class,"MuiSelect-select")])[4]').click()
        cy.get('li[data-value="66"]').click()
        cy.contains('Save & Continue').click()
    }

    addAuctionItems(asset_title,starting_bid,max_bid,min_increment,quantity,description){
        cy.contains('Add your auction items details here').should('exist')
        cy.xpath('//p[text()="Add Assets"]').click()
        cy.contains('Add your assets quickly and efficiently using our simple form').should('exist')
        cy.xpath('//input[@type="text"]').clear().type(asset_title)
        cy.get('input[placeholder*="Starting bid"]').clear().type(starting_bid)
        cy.get('input[placeholder*="Maximum bid"]').clear().type(max_bid)
        cy.get('input[placeholder*="Minimum incremental"]').clear().type(min_increment)
        cy.get('input[type="file"]').attachFile('benz.jpg');
        cy.get('input[placeholder*="Enter quantity"]').clear().type(quantity)
        cy.xpath('//div[contains(@class,"MuiSelect-select")]').click()
        cy.get('li[data-value="New"]').click()
        cy.get('textarea[name="description"]').clear().type(description)
        cy.xpath('(//button[@type="submit"])[2]').click()
        cy.contains('Asset added successfully',{timeout:10000}).should('exist')
        cy.contains('Save & Continue',{timeout:10000}).click()
    }

    addExistingContactList(){
        cy.contains('Select Contact List').click()
        cy.xpath('(//input[@type="checkbox"])[1]').click()
        cy.xpath('//button[@type="submit"]').click()
    }

    createContactList(contact_title, emails){
        cy.contains('Select Contact List').click()
        cy.xpath('//p[text()="Create"]').click()
        cy.get('input[placeholder*="Lagos Staff"]').clear().type(contact_title)
        emails.forEach((email) => {
            cy.get('input[placeholder*="Add emails"]').clear().type(email+"{enter}")
        })
        cy.xpath('(//button[@type="submit"])[2]').click()
        cy.contains('Contact created successfully').should('exist')
        //cy.contains(contact_title).should('be.visible')
        cy.xpath('(//input[@type="checkbox"])[1]').click()
        cy.xpath('//button[@type="submit"]').click()


    }

    uploadContactList(contact_title){
        cy.contains('Select Contact List').click()
        cy.contains('Import Emails').click()
        cy.get('input[type="file"]').attachFile('contact-import.csv');
        cy.contains('Continue').click()
    }

    publishAuction(auction_title){
        cy.contains('Preview your auction details here').should('exist')
        cy.contains(auction_title).should('exist')
        cy.contains('delo@yopmail.com').should('exist')
        cy.contains('lilo@yopmail.com').should('exist')
        cy.contains('Publish').click()
        cy.contains('successfully').should('exist')
    }

    saveAuction(auction_title){
        cy.contains('Preview your auction details here').should('exist')
        cy.contains(auction_title).should('exist')
        cy.contains('delo@yopmail.com').should('exist')
        cy.contains('lilo@yopmail.com').should('exist')
        cy.contains('Save').click()
        cy.contains('Auction saved successfully').should('exist')
    }

    auctionGoLive(auction_title){
        cy.contains('Go Live').click()
        cy.contains('Auction updated successfully',{timeout:3000}).should('exist')
        cy.contains('View Auction',{timeout:3000}).should('exist')
    }

    viewAuction(auction_title){
        cy.contains(auction_title).click()
        cy.contains('Preview your auction details here').should('exist')
        cy.url().should('include','/auctioneer/auction/preview')
    }

    addInvitees(emails){
        cy.contains('Add Invitees').click()
        cy.contains('Send Invitation').click()
        cy.contains('The emails field is required').should('exist')
        emails.forEach((email) => {
            cy.get('input[placeholder*="Add emails"]').clear().type(email+"{enter}")
        })
        cy.contains('Send Invitation').click()
    }

    resendPendingInvites(){
        cy.contains('Resend Pending Invites').click()
    }

    switchToAuctioneePage(){
        cy.xpath('(//button[@aria-label="open drawer"])[1]').click()
        cy.xpath('//span[@aria-label="Switch Account"]').click()
        cy.get('input[value="auctionee"]').click()
        cy.contains('Confirm').click()
        cy.contains('Your current sales auctions and activities').should('exist')
    }

}

export default AuctionsPage
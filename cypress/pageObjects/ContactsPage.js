///<reference types="cypress"/>
import 'cypress-file-upload'

class ContactsPage{

    openContactPage(){
        cy.xpath('(//a[@href="/auctioneer/contact-list"])[1]').click()
        cy.contains('Simplify your contact management').should('exist')
    }

    createContactList(contact_title, emails){
        cy.xpath('//button[text()="Create Contact List"]').click()
        cy.get('input[placeholder*="Lagos Staff"]').clear().type(contact_title)
        emails.forEach((email) => {
            cy.get('input[placeholder*="Add emails"]').clear().type(email+"{enter}")
        })
        cy.xpath('(//button[@type="submit"])').click()
        cy.contains('Contact created successfully').should('exist')
        cy.contains(contact_title).should('be.visible')
    }

}

export default ContactsPage
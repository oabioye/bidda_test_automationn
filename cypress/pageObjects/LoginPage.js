///<reference types="cypress"/>

class LoginPage{
    visit(){
        cy.visit('https://staging.biddahq.com');
    }

    fillEmail(email){
        cy.get('input[type="text"]',{timeout:10000}).should('be.visible')
        cy.get('input[type="text"]').type(email);
    }

    fillPassword(password){
        cy.get('input[type="password"]').type(password);
    }

    submitLoginForm(){
        cy.xpath('//p[text()="Log In"]').click();
    }

    logout(){
        cy.xpath('(//button[@aria-label="open drawer"])[1]').click()
        cy.get(".jss8").click()
        cy.xpath('//button[text()="Confirm"]').click()
    }
}

export default LoginPage
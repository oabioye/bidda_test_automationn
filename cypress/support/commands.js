
import LoginPage from "../pageObjects/LoginPage"

const loginPage = new LoginPage()
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('auctioneer_login', () => {
    loginPage.visit();
    loginPage.fillEmail("obaloluwa.peter@rayda.co")
    loginPage.fillPassword("P@ssword2023")
    loginPage.submitLoginForm()
})

Cypress.Commands.add('auctionee_login', (email, password) => {
    loginPage.visit();
    loginPage.fillEmail(email)
    loginPage.fillPassword(password)
    loginPage.submitLoginForm()
})

Cypress.Commands.add('logout', ()=>{
    loginPage.logout()
})